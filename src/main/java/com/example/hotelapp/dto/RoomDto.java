package com.example.hotelapp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RoomDto {
    private String number;
    private String floor;
    private Double size;
    private Integer hotel_id;
}
