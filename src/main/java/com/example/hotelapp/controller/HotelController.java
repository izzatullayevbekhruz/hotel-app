package com.example.hotelapp.controller;

import com.example.hotelapp.dto.ApiResponse;
import com.example.hotelapp.entity.Hotel;
import com.example.hotelapp.repository.HotelRepository;
import com.example.hotelapp.service.HotelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/hotel")
public class HotelController {
    @Autowired
    HotelRepository hotelRepository;
    @Autowired
    HotelService hotelService;

    @GetMapping
    public ApiResponse getAll() {
        List<Hotel> all = hotelRepository.findAll();
        return new ApiResponse("MAna", true, all);
    }

    @PostMapping("/add")
    public ApiResponse add(@RequestBody Hotel hotel) {
       return hotelService.add(hotel);
    }
    @PostMapping("/edit/{id}")
    public ApiResponse edit(@PathVariable Integer id,@RequestBody Hotel hotel){
        return hotelService.edit(id,hotel);
    }
    @DeleteMapping("/delete/{id}")
    public ApiResponse delete(@PathVariable Integer id){
        return hotelService.delete(id);
    }


}
