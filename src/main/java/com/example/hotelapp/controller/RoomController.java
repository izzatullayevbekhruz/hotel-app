package com.example.hotelapp.controller;

import com.example.hotelapp.dto.ApiResponse;
import com.example.hotelapp.dto.RoomDto;
import com.example.hotelapp.entity.Room;
import com.example.hotelapp.repository.RoomRepository;
import com.example.hotelapp.service.RoomService;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/room")
public class RoomController {


    @Autowired
    RoomRepository roomRepository;
    @Autowired
    RoomService roomService;

    @GetMapping("/list")
    public ApiResponse getAll(){
        List<Room> all = roomRepository.findAll();
        return new ApiResponse("Mana",true,all);
    }
    @PostMapping("/add")
    public ApiResponse add(@RequestBody RoomDto dto){
        return roomService.add(dto);
    }

    @PostMapping("/edit/{id}")
    public ApiResponse edit(@PathVariable Integer id, @RequestBody RoomDto dto){
        return roomService.edit(id,dto);
    }
    @DeleteMapping("/delete/{id}")
    public ApiResponse delete(@PathVariable Integer id){
        return roomService.delete(id);
    }

    @GetMapping
    public Page<Room> all(@RequestParam("num") Integer number){
        Pageable pageable = PageRequest.of(number,2);
        Page<Room> all = roomRepository.findAll(pageable);
        return all;
    }




}
