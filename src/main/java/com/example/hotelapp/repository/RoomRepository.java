package com.example.hotelapp.repository;

import com.example.hotelapp.entity.Room;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface RoomRepository extends JpaRepository<Room,Integer> {

//    List<Room> findByHotelId(Integer id, Pageable pageable);

    @Override
    Page<Room> findAll(Pageable pageable);
}
