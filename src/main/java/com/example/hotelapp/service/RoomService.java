package com.example.hotelapp.service;

import com.example.hotelapp.dto.ApiResponse;
import com.example.hotelapp.dto.RoomDto;
import com.example.hotelapp.entity.Hotel;
import com.example.hotelapp.entity.Room;
import com.example.hotelapp.repository.HotelRepository;
import com.example.hotelapp.repository.RoomRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class RoomService {
    final RoomRepository roomRepository;
    final HotelRepository hotelRepository;

    public ApiResponse add(RoomDto dto) {
        Optional<Hotel> byId = hotelRepository.findById(dto.getHotel_id());
        if (!byId.isPresent()) {
            return new ApiResponse("Xatolik", false);
        }
        for (Room room : roomRepository.findAll()) {
            if (room.getNumber().equals(dto.getNumber())) {
                return new ApiResponse("This room already exist!", false);
            }
        }
        Room room = new Room();
        room.setFloor(dto.getFloor());
        room.setNumber(dto.getNumber());
        room.setSize(dto.getSize());
        room.setHotel(byId.get());
        Room save = roomRepository.save(room);
        return new ApiResponse("Added", true, save);

    }

    public ApiResponse edit(Integer id, RoomDto dto) {
        Optional<Room> byId = roomRepository.findById(id);
        if (!byId.isPresent()) {
            return new ApiResponse("Not found",false);
        }
        Optional<Hotel> byId1 = hotelRepository.findById(dto.getHotel_id());
        Room room = byId.get();
        room.setNumber(dto.getNumber());
        room.setFloor(dto.getFloor());
        room.setSize(dto.getSize());
        room.setHotel(byId1.get());
        Room save = roomRepository.save(room);
        return new ApiResponse("Edit",true,save);
    }

    public ApiResponse delete(Integer id) {
        Optional<Room> byId = roomRepository.findById(id);
        Room room = byId.get();
        roomRepository.delete(room);
        return new ApiResponse("Delete",true);
    }

//    @GetMapping("/tutorials/{id}")
//    public ResponseEntity<Tutorial> getTutorialById(@PathVariable("id") long id) {
//        Optional<Tutorial> tutorialData = tutorialRepository.findById(id);
//
//        if (tutorialData.isPresent()) {
//            return new ResponseEntity<>(tutorialData.get(), HttpStatus.OK);
//        } else {
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        }
//    }


//    Page<Tutorial> findByPublished(boolean published, Pageable pageable);
//
//    Page<Tutorial> findByTitleContaining(String title, Pageable pageable);
//
//    List<Tutorial> findByTitleContaining(String title, Sort sort);
}
