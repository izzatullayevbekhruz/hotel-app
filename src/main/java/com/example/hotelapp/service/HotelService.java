package com.example.hotelapp.service;

import com.example.hotelapp.dto.ApiResponse;
import com.example.hotelapp.entity.Hotel;
import com.example.hotelapp.repository.HotelRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class HotelService {
    final HotelRepository hotelRepository;

    public ApiResponse add(Hotel hotel) {
        for (Hotel hotel1 : hotelRepository.findAll()) {
            if(hotel1.getName().equals(hotel.getName())){
                return new ApiResponse("This hotel already exist!",false);
            }
        }
        Hotel hotel1=new Hotel();
        hotel1.setName(hotel.getName());
        Hotel save = hotelRepository.save(hotel1);
        return new ApiResponse("Added",true,save);
    }

    public ApiResponse edit(Integer id, Hotel hotel) {
        Optional<Hotel> byId = hotelRepository.findById(id);
        if (!byId.isPresent()) {
            return new ApiResponse("Not found",false,null);
        }
        Hotel hotel1 = byId.get();
        hotel1.setName(hotel.getName());
        Hotel save = hotelRepository.save(hotel1);
        return new ApiResponse("Edit",true,save);
    }

    public ApiResponse delete(Integer id) {
        Optional<Hotel> byId = hotelRepository.findById(id);
        if(byId.isPresent()){
            return new ApiResponse("Not found",false);
        }
        Hotel hotel = byId.get();
        hotelRepository.delete(hotel);
        return new ApiResponse("Delete",true);
    }
}
